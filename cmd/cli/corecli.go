//-------------------------------------------------------------------------------
// copyright (c) Aleksey Martynenko aka spouk/cyberspouk spouk@spouk.ru
//-------------------------------------------------------------------------------
package main

import (
	"io"
	"log"
	"os"
	"os/signal"
	"path/filepath"
	"sort"
	"strings"
	"sync"
	"syscall"
	"time"
)

const prefix = "[backuper-server] "

type Core struct {
	log   *log.Logger
	cfg   *Config
	nodes []node

	//мапа для директорий upload
	dirlist map[string]*DirObj

	//количество работающих воркеров
	countWorkers int
	com          chan string //канал для передачи управляющих команд воркерам

	//каналы для отслеживания прерываний
	sigs chan os.Signal
	done chan bool

	sw sync.WaitGroup
}

// NewBackuperServer создаю новый инстанс
func NewBackuperServer(configfile, logfile string) (*Core, error) {
	//make new core instance
	core := &Core{
		log:          log.New(os.Stdout, prefix, log.LstdFlags|log.Lshortfile),
		cfg:          nil,
		nodes:        nil,
		sigs:         make(chan os.Signal, 1),
		done:         make(chan bool, 1),
		sw:           sync.WaitGroup{},
		countWorkers: 0,
		com:          make(chan string),
		dirlist:      make(map[string]*DirObj),
	}

	//open/read config file
	err, cfg := core.ReadtConfigs(configfile)
	if err != nil {
		return core, err
	}
	core.cfg = cfg

	//make logging
	core.log = log.New(os.Stdout, prefix, log.LstdFlags|log.Lshortfile)
	fh, err := core.OpenLogFile(logfile)
	if err != nil {
		return core, err
	}
	core.log.SetOutput(io.MultiWriter(fh, os.Stdout))

	//show config ext arch
	core.log.Printf("-- расширения архивов, которые сервер может обрабатывать %v\n", core.cfg.Extbackup)
	for _, x := range core.cfg.Extbackup {
		core.log.Printf("--архивное расширение из списка: %v\n", x)
	}

	//run catcher signal interrupt
	go core.catcherSigKiller()
	go core.theEnd()
	signal.Notify(core.sigs, syscall.SIGKILL, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP, syscall.SIGKILL, syscall.SIGABRT)

	//проверка наличия директорий
	if err := core.checkExistsDirs(); err != nil {
		return nil, err
	}

	//выстраиваю дерево каталога upload
	core.log.Printf("--запускаю синхронизацию директорий...")
	stockMap, err := core.createTreeDirSync()
	if err != nil {
		return core, err
	}
	core.dirlist = stockMap

	core.log.Printf("--stockMap: %v\n", stockMap)

	//os.Exit(1)

	//return result
	return core, nil
}

func (core *Core) theEnd() {
	<-core.done
	core.log.Printf("-- удачи и успехов --")
	os.Exit(1)
}

//перехват прерываний
func (core *Core) catcherSigKiller() {
	//поймали прерывание, завершаем все делишки
	sig := <-core.sigs
	core.log.Printf("[catcherSigKiller][WARNING] пойман сигнал прерывания работы сервера `%v`,  что не есть хорошо!, подождите немного пока я корректно завершу работу, ок? я так и думал... ", sig)

	//отправка команды о прекращении работы воркерам
	core.com <- "exit"

	time.Sleep(time.Second * 2)
	//отправляю сигнал об успешной обработке выхода
	core.done <- true
	return
}

// StartServer запуск сервера бэкапера
func (core *Core) StartServer() {
	core.log.Printf("--start server...")
	defer func() {
		core.log.Printf("--server end working...")
	}()

	//запуск воркеров
	core.countWorkers = 1
	for alias, objDir := range core.dirlist {
		if core.cfg.Debug {
			core.log.Printf("--key: %v\n", alias)
		}
		//core.sw.Add(1)
		go core.Worker(core.countWorkers, objDir)
		core.countWorkers++
	}

	//запуск воркера-наблюдателя
	core.sw.Add(1)
	go core.workerListenerDir()

	//режим ждуна
	ch := make(chan bool)
	<-ch
	//core.sw.Wait()

	return
}

//контроль за уровнем
func (core *Core) controlbackupready(aliasdb string) error {
	info := NewInfoDirectory(core.log)

	//сканирование директории с готовыми бэкапами
	err := info.ParseDirectory(filepath.Join(core.cfg.Dirremotebackup, aliasdb), filepath.Join(core.cfg.Dirreadybackup, aliasdb))
	if err != nil {
		return err
	}

	//получени данных
	res, err := info.Get(info.READY, aliasdb)
	if err != nil {
		return err
	}
	//отбор только архивов
	onlyarc := []*FileElement{}

	for _, x := range res {
		for _, z := range core.cfg.Extbackup {
			if x.Extension == z {
				onlyarc = append(onlyarc, x)
				break
			}
		}
	}

	//сортировка
	sort.Slice(onlyarc, func(i, j int) bool {
		return onlyarc[i].item.ModTime().UnixNano() > onlyarc[j].item.ModTime().UnixNano()
	})

	//удаление
	if len(res) > core.cfg.Level {
		for _, x := range onlyarc[core.cfg.Level:] {
			//удаляю файл 7z
			core.log.Printf("[%v][control] removing file -->   %v ", x.AliasDB, x.Fullpath)
			err := os.Remove(x.Fullpath)
			if err != nil {
				core.log.Printf("[%v][control] removing file error -->   %v ", x.AliasDB, x.Fullpath, err)
			}
			//удаляю файл json
			dstjson := x.Pathonly + "/" + x.FilanameOnly + ".json"
			core.log.Printf("[%v][control] removing file -->   %v ", x.AliasDB, dstjson)
			err = os.Remove(dstjson)
			if err != nil {
				core.log.Printf("[%v][control] removing file error -->   %v ", x.AliasDB, dstjson, err)
			}
		}
	}
	return nil
}

//преобразует путь до новой директории в алиас из remotebackup + создает директорию в ready при ее отсутствии
func (core *Core) GetAliasPathMakeReady(path string) (*DirObj, error) {
	var obj = &DirObj{}
	clean := filepath.Clean(path)
	aliasstock := strings.Split(clean, "/")
	alias := aliasstock[len(aliasstock)-1:][0]
	ralias := filepath.Join(core.cfg.Dirreadybackup, alias)
	obj.Alias = alias
	obj.ReadyDir = filepath.Join(core.cfg.Dirreadybackup, alias)
	obj.UploadDir = filepath.Join(core.cfg.Dirremotebackup, alias)
	obj.WorkerID = -1
	_, err := os.Stat(ralias)
	if os.IsNotExist(err) {
		//не найдена, создаю
		err = os.Mkdir(ralias, os.ModeDir|os.ModePerm)
		if err != nil {
			core.log.Printf("--[error][%v] ошибка создания директории %v \n", alias, err)
			return nil, err
		}
		//изменяю uid/gid
		err = os.Chown(obj.ReadyDir, core.cfg.Uid, core.cfg.Gid)
		if err != nil {
			return nil, err
		} else {
			core.log.Printf("--успешно изменены uid:gid на директорию %v\n", core.cfg.Uid, core.cfg.Gid, obj.ReadyDir)
		}
	}
	return obj, nil
}

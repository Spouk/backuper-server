//-------------------------------------------------------------------------------
// copyright (c) Aleksey Martynenko aka spouk/cyberspouk spouk@spouk.ru
//-------------------------------------------------------------------------------
package main

import (
	"flag"
	"fmt"
	"io/fs"
	"os"
	"path/filepath"
	"strings"
	"time"
)

var version = "1.0.3"

func main() {
	//
	//ff, err := ioutil.ReadDir("/var/log")
	//if err != nil {
	//	log.Fatal(err)
	//}
	//for _, x := range ff {
	//	fu := filepath.Base(x.Name())
	//	filepath.
	//	log.Printf("-%v : %v\n", x.Name(), fu)
	//}
	//os.Exit(1)

	//vv := "/mnt/ftp/upload/t022`"
	////res := filepath.Base(vv) //t022
	//dir, file := filepath.Split(vv)
	//fmt.Printf("result: %v : %v\n", dir, file)
	//
	////check dir stat
	//du := "/tmp/hackertest"
	//finfo, err := os.Stat(du)
	//if err != nil {
	//	fuerr := err.(*fs.PathError)
	//	log.Printf("%v %v %v\n", fuerr.Err, fuerr.Path, fuerr.Op)
	//	err = os.Mkdir(du, 0777)
	//	if err != nil {
	//		log.Fatal(err)
	//	} else {
	//		log.Println("success creating directory ", du)
	//	}
	//} else {
	//	fmt.Printf("%v\n", finfo)
	//
	//}
	//os.Exit(1)

	var (
		cf = flag.String("config", "", "путь к кофигу с полным путем ")
		lf = flag.String("log", "", "полный путь с именем лога")
	)
	flag.Parse()
	if *cf == "" || *lf == "" {
		var line []string
		for x := 0; x < 45; x++ {
			line = append(line, "=")
		}
		fmt.Printf("%v\n Backuper-server [c] aleksey.martynenko// cyberspouk@gmail.com ||"+
			"spouk@spouk.ru ,  version %v\n%v\n\n", strings.Join(line, " "), version, strings.Join(line, " "))
		flag.PrintDefaults()
		os.Exit(1)
	}

	//создаю новый инстанс с одновременной инициализацией
	core, err := NewBackuperServer(*cf, *lf)
	if err != nil {
		core.log.Printf("fatalerror: %v", err)
		os.Exit(1)
	}
	core.log.Printf("cfg: %#v", core.cfg)

	//запуск сервера
	core.StartServer()
}
func clearemptyfiles(indir string) {
	var currentDir string
	_ = filepath.Walk(indir, func(path string, info fs.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if info.IsDir() {
			currentDir = info.Name()
			fmt.Printf("%v\n", currentDir)
		}
		if !info.IsDir() {
			if time.Now().Sub(info.ModTime()).Minutes() >= 60 && info.Size() == 0 {
				fmt.Printf("--file need remove, file exists after 60 min  %v %v %v\n", info.Name(), info.Size(), path)
			} else {
				fmt.Printf("--file no need remove, its new file %v\n", info.Name())
			}
		}
		return nil
	})
}

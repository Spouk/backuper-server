//-------------------------------------------------------------------------------
// copyright (c) Aleksey Martynenko aka spouk/cyberspouk spouk@spouk.ru
//-------------------------------------------------------------------------------
package main

import (
	"encoding/json"
	"fmt"
	"github.com/go-yaml/yaml"
	"io"
	"io/fs"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"sort"
	"strings"
	"time"
)

//---------------------------------------------------------------------------------------------------
// функции для предподготовки и запуска сервера*/
//----------------------------------------------------------------------------------------------------
//выстраиваю дерево объектов - директорий для наблюдения и делаю синхронизацию с upload<->ready
func (core *Core) createTreeDirSync() (map[string]*DirObj, error) {
	//парсю upload директорию
	var stock = make(map[string]*DirObj)
	err := filepath.Walk(core.cfg.Dirremotebackup, func(path string, info fs.FileInfo, err error) error {
		if err != nil {
			core.log.Fatal(err)
		}
		_, fuckpath := filepath.Split(path)
		_, fuckpath2 := filepath.Split(core.cfg.Dirremotebackup)

		if strings.Compare(fuckpath, fuckpath2) == 0 {
			core.log.Printf("--[info] директория корневая далеее идем...\n")
			return nil
		}

		if info.IsDir() {
			_, alias := filepath.Split(path)
			uploadAlias := filepath.Join(core.cfg.Dirreadybackup, alias)
			core.log.Printf("--found alias: %v : %v\n", alias, uploadAlias)
			t := &DirObj{
				Alias:     alias,
				UploadDir: path, //fullpath with alias
				ReadyDir:  uploadAlias,
				WorkerID:  -1,
			}
			stock[info.Name()] = t

			//проверяю на наличие директории по upload и создаю если не найдено
			_, err := os.Stat(uploadAlias)
			if err != nil && os.IsNotExist(err) {
				//создаю директорию
				err = os.Mkdir(uploadAlias, os.ModeDir|os.ModePerm)
				if err != nil {
					return err
				} else {
					core.log.Printf("--успешно создана директория %v\n", uploadAlias)
					//изменяю uid/gid
					err = os.Chown(uploadAlias, core.cfg.Uid, core.cfg.Gid)
					if err != nil {
						return err
					} else {
						core.log.Printf("--успешно изменены uid:gid на директорию %v\n", core.cfg.Uid, core.cfg.Gid, uploadAlias)
					}
				}
			} else {
				return err
			}
		}
		return nil
	})
	if err != nil {
		return nil, err
	}

	// возвращаю результат
	for k, v := range stock {
		core.log.Printf("%v %v\n", k, v)
	}
	return stock, nil

}

//проверка наличия входящей и исходящей директорий
func (core *Core) checkExistsDirs() error {
	for _, d := range []string{core.cfg.Dirfail, core.cfg.Dirreadybackup, core.cfg.Dirstat, core.cfg.Dirremotebackup} {
		_, err := os.Stat(d)
		if err != nil && os.IsNotExist(err) {
			core.log.Printf("директория не найдена по указанному пути -> %v, создаю с uid/gid из конфига", d)
			err = os.Mkdir(d, os.ModeDir|os.ModePerm)
			if err != nil {
				return err
			} else {
				core.log.Printf("директория успешно создана %v\n", d)
				err = os.Chown(d, core.cfg.Uid, core.cfg.Gid)
				if err != nil {
					return err
				} else {
					core.log.Printf("директории %v успешно изменены gid:uid: %v:%v\n", d, core.cfg.Gid, core.cfg.Uid)
				}
			}
		}
	}
	return nil
}

// OpenLogFile open/create log file
func (core *Core) OpenLogFile(filename string) (*os.File, error) {

	//c.Log.Write(statusInfo, fmt.Sprintf("filename log: %s", filename))
	if stat, err := os.Stat(filename); err != nil {
		if err == os.ErrNotExist {
			fh, err := os.OpenFile(filename, os.O_CREATE|os.O_WRONLY, os.ModePerm)
			if err != nil {
				return nil, err
			}
			return fh, nil
		} else {

			if _, ok := err.(*fs.PathError); ok {
				fh, err := os.OpenFile(filename, os.O_CREATE|os.O_WRONLY, os.ModePerm)
				if err != nil {
					return nil, err
				}
				return fh, nil
			}
			return nil, err
		}
	} else {
		if !stat.IsDir() {
			fh, err := os.OpenFile(filename, os.O_APPEND|os.O_WRONLY, os.ModePerm)
			if err != nil {
				return nil, err
			}
			return fh, nil
		}
	}
	return nil, nil
}

//prepare data:month, day for adding 00
func (core *Core) timeDatePrepare(res int) string {
	if res <= 9 {
		return fmt.Sprintf("0%d", res)
	}
	return fmt.Sprintf("%d", res)

}

//check exist file/dir
func (core *Core) exists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return false, err
}

// ReadtConfigs read and return config and node struct
func (core *Core) ReadtConfigs(conffile string) (error, *Config) {
	//init variables
	var (
		cfg = Config{}
	)

	//open and read yaml files
	for k, v := range map[string]interface{}{conffile: &cfg} {
		if err := core.readyamlfile(k, v); err != nil {
			core.log.Printf("error read yaml file %v %v", k, err)
			return err, nil
		}
	}
	return nil, &cfg
}

//correct time run from string to time.time
func (core *Core) correctTimerun(val string) (time.Time, error) {
	layer := "15:04:05"
	startime, err := time.Parse(layer, val)
	if err != nil {
		return time.Now(), err
	}
	return startime, nil
}

//read yaml file
func (core *Core) readyamlfile(f string, c interface{}) error {
	//open yaml file
	fh, err := os.Open(f)
	if err != nil {
		return err
	}
	dec := yaml.NewDecoder(fh)
	err = dec.Decode(c)
	if err != nil {
		return err
	}
	return nil
}

//read json file
func (core *Core) readjsonfile(f string, c interface{}) error {
	//open json file
	fh, err := os.Open(f)
	if err != nil {
		return err
	}
	dec := json.NewDecoder(fh)
	err = dec.Decode(c)
	if err != nil {
		return err
	}
	return nil
}

//---------------------------------------------------------------------------------------------------
// функции для поддержки работы воркеров*/
//----------------------------------------------------------------------------------------------------
// copy from use core syscall: лучший способ по скорости перемещения файла
func (core *Core) moveFile(src, dst string) error {
	err := os.Rename(src, dst)
	if err != nil {
		return err
	}
	return nil
}
func (core *Core) MoveFileIn(sourcePath, destPath string) error {
	inputFile, err := os.Open(sourcePath)
	if err != nil {
		return fmt.Errorf("Couldn't open source file: %s", err)
	}
	outputFile, err := os.Create(destPath)
	if err != nil {
		inputFile.Close()
		return fmt.Errorf("Couldn't open dest file: %s", err)
	}
	defer outputFile.Close()
	_, err = io.Copy(outputFile, inputFile)
	inputFile.Close()
	if err != nil {
		return fmt.Errorf("Writing to output file failed: %s", err)
	}
	// The copy was successful, so now delete the original file
	err = os.Remove(sourcePath)
	if err != nil {
		return fmt.Errorf("Failed removing original file: %s", err)
	}
	return nil
}
func (core *Core) MoveFileSystemMVLinux(src, dst string) error {
	//exec.Command("/bin/sh", "-c", "mv ./source_dir/* ./dest_dir")
	cmd := exec.Command("/bin/sh", "-c", fmt.Sprintf("mv %s %s", src, dst))
	if err := cmd.Run(); err != nil {
		return err
	}
	return nil
}

// копирование из src -> dst
func (core *Core) copyready(aliasdb, src, dest string) error {
	buf := make([]byte, 1024)
	countread, countwrite := 0, 0
	stat, err := os.Stat(src)
	if err != nil {
		return err
	}
	core.log.Printf("[%v][copyready] input file %v size = %v", aliasdb, src, stat.Size())
	in, err := os.Open(src)
	if err != nil {
		return err
	}
	defer func(in *os.File) {
		err := in.Close()
		if err != nil {
			core.log.Printf("[%v][copyready] error close handler  file %v ", aliasdb, src)
		}
	}(in)
	out, err := os.Create(dest)
	if err != nil {
		return err
	}
	defer func(out *os.File) {
		err := out.Close()
		if err != nil {
			core.log.Printf("[%v][copyready] error close handler  file %v ", aliasdb, dest)
		}
	}(out)

	for {
		n, err := in.Read(buf)
		countread += n
		if err != nil && err != io.EOF {
			return err
		}
		if n == 0 {
			break
		}
		nw, err := out.Write(buf[:n])
		if err != nil {
			return err
		}
		countwrite += nw
	}
	core.log.Printf("[%v][copyready] success copy file from src: %v => dest: %v, countread: %v, countwrite: %v",
		aliasdb, src, dest, countread, countwrite)

	return nil
}

//функция по контролю за глубиной вложенности бэкапов отдельной директории
func (core *Core) controlBackupsAlias(obj *DirObj) error {

	//читаю директорию где хранятся бэкапы по алиасу
	stockFile, err := ioutil.ReadDir(obj.ReadyDir)
	if err != nil {
		return err
	}

	//анонимная функция - чекер
	checker := func(ext string) bool {
		for _, x := range core.cfg.Extbackup {
			if ext == x {
				return true
			}
		}
		return false
	}

	//отбираю только те файлы , которые входят архивным расширением в список допустимых + наличие json файла пары
	var stockValidate []fs.FileInfo
	for _, x := range stockFile {
		if !x.IsDir() {
			fileExt := filepath.Ext(x.Name())
			if checker(fileExt) {
				stockValidate = append(stockValidate, x)
			}
		}
	}

	//сортировка получившегося списка из архивов
	sort.Slice(stockValidate, func(i, j int) bool {
		return stockValidate[i].ModTime().UnixNano() > stockValidate[j].ModTime().UnixNano()
	})

	//удаляю бэкапы согласно уровню вложенности из конфига;; при условии превышения количества
	if len(stockValidate) > core.cfg.Level {
		for _, x := range stockValidate[core.cfg.Level:] {

			//удаляется файл с архивом
			fpath := filepath.Join(obj.ReadyDir, x.Name())
			core.log.Printf("[%v][controlBackup] удаление файла -->   %v ", obj.Alias, fpath)
			err := os.Remove(fpath)
			if err != nil {
				core.log.Printf("[%v][controlbackupdeep] error remove file -->  %v %v ", obj.Alias, fpath, err)
			}

			//remove json

			filename := strings.TrimSuffix(x.Name(), filepath.Ext(x.Name()))
			filenamejson := filename + ".json"
			fpathjson := filepath.Join(obj.ReadyDir, filenamejson)
			core.log.Printf("--json данные: %v %v %v\n", filename, filenamejson, fpathjson)
			core.log.Printf("[%v][controlBackupDEEP] removing file json -->   %v ", obj.Alias, fpathjson)
			err = os.Remove(fpathjson)
			if err != nil {
				core.log.Printf("[%v][controlBackupDEEP] error remove file  json -->  %v %v ", obj.Alias, fpathjson, err)
			}
		}
	}

	return nil
}

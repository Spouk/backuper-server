//-------------------------------------------------------------------------------
// copyright (c) Aleksey Martynenko aka spouk/cyberspouk spouk@spouk.ru
//-------------------------------------------------------------------------------
package main

import (
	"github.com/fsnotify/fsnotify"
	"io/fs"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"
	"time"
)

//воркер по перемещению готовых и проверенныъ json + 7z на постоянное место хранения
func (core *Core) workerReadymove(cp copymove) {
	var (
		rp7z   bool
		rpjson bool
		mv7z   bool
		mvjson bool
	)
	//копирую файлы + проверка наличия файла в удаленной директории
	if _, err := os.Stat(cp.dst7z); err != nil && os.IsNotExist(err) {
		//сначала пробую перенос (move) файла, это сработает в случае наличия одной файловой системы (один диск к примеру)
		//если не сработает выйдет с ошибкой то использую копирование
		err := core.moveFile(cp.src7z, cp.dst7z)
		if err != nil {
			core.log.Printf("[%v] [movefile] error move file %v -> %v", cp.aliasdb, cp.src7z, err)
			err := core.copyready(cp.aliasdb, cp.src7z, cp.dst7z)
			if err != nil {
				core.log.Printf("[%v] [copymove] error copy file %v -> %v", cp.aliasdb, cp.src7z, err)
				rp7z = false
			} else {
				core.log.Printf("[%v] [copymove] success copy file %v -> %v", cp.aliasdb, cp.src7z, cp.dst7z)
				rp7z = true
			}
		} else {
			mv7z = true
			rp7z = true
			core.log.Printf("[%v] [movefile] success move file %v -> %v", cp.aliasdb, cp.src7z, cp.dst7z)
		}
	} else {
		core.log.Printf("[%v] [copymove] already exists, skippping  %v -> %v", cp.aliasdb, cp.src7z, cp.dst7z)
		rp7z = true
	}
	if _, err := os.Stat(cp.dstjson); err != nil && os.IsNotExist(err) {
		err := core.moveFile(cp.srcjson, cp.dstjson)
		if err != nil {
			err = core.copyready(cp.aliasdb, cp.srcjson, cp.dstjson)
			if err != nil {
				core.log.Printf("[%v] [copymove] error copy file %v -> %v", cp.aliasdb, cp.srcjson, err)
				rpjson = false
			} else {
				core.log.Printf("[%v] [copymove] success copy file %v -> %v", cp.aliasdb, cp.srcjson, cp.dstjson)
				rpjson = true
			}
		} else {
			core.log.Printf("[%v] [movefile] success move file %v -> %v", cp.aliasdb, cp.srcjson, cp.dstjson)
			rpjson = true
			mvjson = true
		}
	} else {
		core.log.Printf("[%v] [copymove] already exists, skipping %v -> %v", cp.aliasdb, cp.srcjson, cp.dstjson)
		rpjson = true
	}
	//если были ошибки при копировании файлов то копирую json в fail
	if !rp7z || !rpjson {
		_, f := filepath.Split(cp.srcjson)
		fpath := filepath.Join(core.cfg.Dirfail, f)
		err := core.moveFile(cp.srcjson, fpath)
		if err != nil {
			core.log.Printf("[%v] [copymove] error move file %v -> %v, %v", cp.aliasdb, cp.srcjson, fpath, err)
			err := core.copyready(cp.aliasdb, cp.srcjson, fpath)
			if err != nil {
				core.log.Printf("[%v] [copyready] error copy file %v -> %v, %v", cp.aliasdb, cp.srcjson, fpath, err)
			} else {
				core.log.Printf("[%v] [copyready] success copy file %v -> %v, %v", cp.aliasdb, cp.srcjson, fpath, err)
			}
		} else {
			core.log.Printf("[%v] [copymove] success move file %v -> %v, %v", cp.aliasdb, cp.srcjson, fpath, err)
		}
	}
	//удаляю файлы
	if rp7z && rpjson && !mvjson && !mv7z {
		err := os.Remove(cp.src7z)
		if err != nil {
			core.log.Printf("[%v] [copymove] error delete  file %v -> %v", cp.aliasdb, cp.src7z, err)
		} else {
			core.log.Printf("[%v] [copymove] success delete file %v ", cp.aliasdb, cp.src7z)
		}
		err = os.Remove(cp.srcjson)
		if err != nil {
			core.log.Printf("[%v] [copymove] error delete file %v -> %v", cp.aliasdb, cp.srcjson, err)
		} else {
			core.log.Printf("[%v] [copymove] success delete file %v ", cp.aliasdb, cp.srcjson)
		}
	}
}

//воркер выполняющий работу по обработке бэкапов ;; dirbackup, dirready - key in InfoDirectory; конкретные директории по которым надо вести обработку
//func (core *Core) Worker(number int, key string) {
func (core *Core) Worker(number int, obj *DirObj) {
	core.log.Printf("--run single worker #%v start...", number)
	defer func() {
		//core.sw.Done()
		core.log.Printf("--end work single worker #%v", number)
		return
	}()

	core.log.Printf("--dirobj: %v\n", obj)

	//тикер для проверки с периодичностью
	ticker := time.NewTicker(time.Second * core.cfg.Timeperiodcheck)

	//анонимная функция - чекер
	checker := func(ext string) bool {
		for _, x := range core.cfg.Extbackup {
			if ext == x {
				return true
			}
		}
		return false
	}

	//бесконечный цикл
	for {
		select {
		case _ = <-ticker.C:
			core.log.Printf("--start checking .....%v\n", number)
			//контроль за уровнем вложенности бэкапов
			err := core.controlBackupsAlias(obj)
			if err != nil {
				if strings.Contains(err.Error(), "is nil") {
					core.log.Printf("--[worker%v][%v] директорию удалили, заканчиваю работу :%v..\n", number, obj.Alias, err)
					return
				} else {
					core.log.Printf("--[worker%v][%v] error control deep backups :%v..\n", number, obj.Alias, err)
				}
			}

			//сбор данных по dirObj файлам
			//отбираю только те файлы , которые входят архивным расширением в список допустимых + наличие json файла пары
			var (
				infiles  []fs.FileInfo
				outfiles []fs.FileInfo
			)

			//читаю директорию куда постуют бэкапы по алиасу
			stockFile, err := ioutil.ReadDir(obj.UploadDir)
			if err != nil {
				core.log.Printf("--error cant read directory %v %v\n", obj.UploadDir, err)
			}
			//upload dir
			for _, x := range stockFile {
				if !x.IsDir() {
					fileExt := filepath.Ext(x.Name())
					if checker(fileExt) || fileExt == ".json" {
						infiles = append(infiles, x)
					}
				}
			}
			core.log.Printf("infile: %v\n", infiles)

			//читаю директорию где хранятся бэкапы по алиасу
			stockFile, err = ioutil.ReadDir(obj.ReadyDir)
			if err != nil {
				core.log.Printf("--error cant read directory %v %v\n", obj.ReadyDir, err)
			}
			//upload dir
			for _, x := range stockFile {
				if !x.IsDir() {
					fileExt := filepath.Ext(x.Name())
					if checker(fileExt) {
						outfiles = append(outfiles, x)
					}
				}
			}
			core.log.Printf("outfiles: %v\n", outfiles)

			//проверка наличия json infiles и если json есть открыть его и если ок то проверка по размеру архива и если ок то перенос
			//чтение json файлов в filebackup и перемещение их на место fileready
			//бегу по списку файлов, отбираю только json
			for _, f := range infiles {
				//json
				ext := filepath.Ext(f.Name())
				filename := strings.TrimSuffix(f.Name(), filepath.Ext(f.Name()))
				core.log.Printf("check file: %v:%v\n", filename, ext)

				if ext == ".json" {
					if core.cfg.Debug {
						core.log.Printf("--[worker%v][%v] open json file: %v\n", number, obj.Alias, f.Name())
					}
					//открываю json файл
					inode := infoNode{}
					filenamejson := filename + ".json"
					fpathjson := filepath.Join(obj.UploadDir, filenamejson)
					core.log.Printf("found json file: %v:%v\n", filenamejson, fpathjson)

					if err := core.readjsonfile(fpathjson, &inode); err != nil {
						core.log.Printf("--[worker%v][%v] error open json file: %v :%v\n", number, obj.Alias, filename, err)
					} else {
						//json успешно открыт, файл не битый структура прочитана, добавляю в сток на обработку
						core.log.Printf("--[worker%v][%v] success open json file: %v :%v\n", number, obj.Alias, filename)

						//ищу архив по имени .json файла;; он должен быть уже в слайсе файлов, если нет то скипаю
						foundArc := false

						for _, fz := range infiles {
							//если архив найден по json файлу
							if fz.Name() == inode.Info7z.Name {
								core.log.Printf("--[worker%v][%v] found archive for json file: %v :%v\n", number, obj.Alias, inode.Info7z.Name)
								foundArc = true

								//проверяю размер архива по факту и из json
								if fz.Size() == inode.Info7z.Size {
									core.log.Printf("--[worker%v][%v] size archive = size json file : %v :%v\n", number, obj.Alias, fpathjson)

									//делаю перенос архива с json файлом в readydir
									cp := copymove{
										aliasdb: obj.Alias,
										src7z:   filepath.Join(core.cfg.Dirremotebackup, obj.Alias, fz.Name()),
										dst7z:   filepath.Join(core.cfg.Dirreadybackup, obj.Alias, fz.Name()),
										srcjson: fpathjson,
										dstjson: filepath.Join(core.cfg.Dirreadybackup, obj.Alias, filenamejson),
									}
									core.log.Printf("--[worker%v][%v]  cp :%v\n", number, obj.Alias, cp)
									core.workerReadymove(cp)
								}

							}
						}
						if !foundArc {
							core.log.Printf("--[worker%v][%v] NOT found archive for json file: %v :%v....continue\n", number, obj.Alias, fpathjson)
							continue
						}
					}
				}
			}
		default:
			continue
		}
	}
}

//воркер наблюдатель за remotebackup  и создающий воркеров для наблюдения за новыми директориями
func (core *Core) workerListenerDir() {
	defer func() {
		core.log.Println("-- workerListenerDir работу закончил....")
	}()
	core.log.Printf("--workerListenerDir запущен... -> %v\n", core.cfg.Dirremotebackup)

	// Create new watcher.
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		core.log.Fatalf("-- workerListenerDir фатальная ошибка %v\n", err)
		return
	}
	defer func(watcher *fsnotify.Watcher) {
		err := watcher.Close()
		if err != nil {
			log.Println(err)
		}
	}(watcher)

	err = watcher.Add(core.cfg.Dirremotebackup)
	if err != nil {
		log.Println("error set directory for watching!!!!!, exit listener....")
		return
	}

	//бесконечный цикл в рамках которого происходит наблюдение на upload директорией
	for {
		select {
		case command := <-core.com:
			switch command {
			case "exit":
				core.log.Printf("--getting command to exit, cya dude ;)")
				return
			}
		case event, ok := <-watcher.Events:
			if !ok {
				core.log.Println("-- error get events from watcher...")
				continue
			}
			if core.cfg.Debug && core.cfg.Debugline {
				core.log.Println("event:", event)
			}
			if event.Has(fsnotify.Create) {
				if core.cfg.Debug && core.cfg.Debugline {
					core.log.Println("create file/dir:", event.Name)
				}
				stat, err := os.Stat(event.Name)
				if err != nil {
					core.log.Println("error get stat ", event.Name, err)
					continue
				}
				if stat.IsDir() {
					core.log.Println("bingo, this directory: ", event.Name)

					//формирую алиас и создаю директорию в readybackup если ее там нет
					objDir, err := core.GetAliasPathMakeReady(event.Name)
					if err != nil {
						core.log.Printf("----FATAL.ERROR: %v\nиз-за этой ошибки не запущен воркер для обработки по этой директории %v\n", err, objDir.UploadDir)
					} else {
						//добавляю в общую мапу
						core.dirlist[objDir.Alias] = objDir

						//запускаю воркера по новой директории
						core.countWorkers++

						//запускаю воркера для новой директории
						core.sw.Add(1)
						go core.Worker(core.countWorkers, objDir)
					}
				}
			}
		case err, ok := <-watcher.Errors:
			if !ok {
				log.Println("-- error get events from watcher...", err)
				continue
			}
		}
	}

}

// горутина, которая сканирует с периодчностью директорию .../ready/*, анализирует файла на предмет размера ,
//если файл имеет 0 размер и дата создания превышает более 60 минут, то файл удаляется
func (core *Core) workeclearEmptyFiles() {
	core.log.Printf("--[workerclearemptyfiles] running....")
	defer func() {
		core.log.Printf("--[workerclearemptyfiles] end work....")
		return
	}()
	//тикер для проверки с периодичностью
	ticker := time.NewTicker(time.Second * core.cfg.Timeperiodcheck)

	//бесконечный цикл
	for {
		select {
		case _ = <-ticker.C:

			//парсю upload директорию
			_ = filepath.Walk(core.cfg.Dirremotebackup, func(path string, info fs.FileInfo, err error) error {
				if err != nil {
					return err
				}
				if !info.IsDir() {
					if time.Now().Sub(info.ModTime()).Minutes() >= 60 && info.Size() == 0 {
						err := os.Remove(path)
						if err != nil {
							core.log.Printf("--error remove zerro file %v: %v\n", path, err)
						}
					}
				}
				return nil
			})

		default:
			continue
		}
	}
}

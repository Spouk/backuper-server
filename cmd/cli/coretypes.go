package main

import (
	"errors"
	"fmt"
	"io/fs"
	"log"
	"os"
	"path/filepath"
	"strings"
	"sync"
	"time"
)

//структура для директории upload
type DirObj struct {
	Alias     string //алиас загружаемой базы
	UploadDir string //полный путь к директории куда загружаются бэкапы
	ReadyDir  string //полный путь к директории куда помещаются бэкапы
	WorkerID  int    //идентификатор воркера который смотрит за этим алиасом / директориями
}
type ControlBackupFile struct {
	filename string      //filename with ext
	fileext  string      //extenstion file
	fsde     fs.DirEntry //no comment
	aliasdb  string      // alias db: aliasdir :  tester
	basepath string      //fullpath to files : /stock/ready/tester/
	corepath string      //core path: /stock/ready <- from core.cfg.Dirreaydbackup
	info     fs.FileInfo //fileinfo
	fullpath string      //fullpath with filename
	jsonfile string      //filename with .json ext
}

type (
	Config struct {
		Debug           bool          `yaml:"debug"`
		Debugline       bool          `yaml:"debugline"`
		Logfile         string        `yaml:"logfile"`
		Level           int           `yaml:"level"`
		Extbackup       []string      `yaml:"extbackup"`
		Dirremotebackup string        `yaml:"dirremotebackup"`
		Dirreadybackup  string        `yaml:"dirreadybackup"`
		Dirstat         string        `yaml:"dirstat"`
		Dirfail         string        `yaml:"dirfail"`
		Removeerrjson   bool          `yaml:"removeerrjson"`
		Removecountry   int           `yaml:"removecountry"`
		Timerun         []string      `yaml:"timerun"`
		CorrectTimerun  []time.Time   `yaml:"correcttimerun"`
		Timeperiodcheck time.Duration `yaml:"timeperiodcheck"`
		Removeerr7z     bool          `yaml:"removeerr7z"`
		Countworkers    int           `yaml:"countworkers"`
		Uid             int           `yaml:"uid"`
		Gid             int           `yaml:"gid"`
	}
)

type node struct {
	Aliasdb  string `json:"aliasdb"`
	Comment  string `json:"comment"`
	Fullpath string `json:"fullpath"`
}

type infoNode struct {
	Jsonfilename string `json:"jsonfilename"`
	ServerBackup string `json:"server_backup"` //server where making backup and run backuper
	ServerDB     string `json:"server_db"`     //ip server dbs
	Aliasdb      string `json:"aliasdb"`
	Counts       int    `json:"counts"` // count archive backups save
	Mode         string `json:"mode"`
	Typeos       string `json:"typeos"`
	InfoFbk      struct {
		Name      string `json:"name"` //namecreate
		Size      int64  `json:"size"`
		Hash      string `json:"hash"`
		StartTime string `json:"start_time"`
		EndTime   string `json:"end_time"`
		LogFile   string `json:"log_file"`
	} `json:"info_fbk"`
	InfoRestore struct {
		Name      string `json:"name"` //namecreate
		Size      int64  `json:"size"`
		Hash      string `json:"hash"`
		StartTime string `json:"start_time"`
		EndTime   string `json:"end_time"`
		LogFile   string `json:"log_file"`
	} `json:"info_restore"`
	Info7z struct {
		Name      string `json:"name"` //namearc
		Size      int64  `json:"size"`
		Hash      string `json:"hash"` //namehash
		StartTime string `json:"start_time"`
		EndTime   string `json:"end_time"`
	} `json:"info_7_z"`
}
type report struct {
	isdir    bool
	worker   string
	event    string
	filename string
	err      error
}

//перемещение файла , работает корректно только в рамках одного диска
type copymove struct {
	aliasdb string
	src7z   string
	dst7z   string
	srcjson string
	dstjson string
}
type FileElement struct {
	AliasDB      string //last element with filepath.dir(filepath.split()))
	Fullpath     string //abs path with filename
	Pathonly     string //filepath.dir with clean
	Extension    string //file extension
	Filename     string //filename with extension
	FilanameOnly string //filename without extension
	item         fs.FileInfo
}
type InfoDirectory struct {
	Dirremotebackup                   map[string][]*FileElement
	Dirreadybackup                    map[string][]*FileElement
	log                               *log.Logger
	sync.Mutex                        `json:"sync.Mutex"`
	REMOTE, READY                     string
	lastremotebackup, lastreadybackup string
}

func NewInfoDirectory(log *log.Logger) *InfoDirectory {
	return &InfoDirectory{
		Dirremotebackup: make(map[string][]*FileElement),
		Dirreadybackup:  make(map[string][]*FileElement),
		log:             log,
		REMOTE:          "REMOTE",
		READY:           "READY",
	}
}

//ex : Get(InfoDirectory.REMOTE, keymap)
func (i *InfoDirectory) Get(typeDir, key string) ([]*FileElement, error) {
	i.Lock()
	defer i.Unlock()
	switch typeDir {
	case i.READY:
		res, exists := i.Dirreadybackup[key]
		if !exists {
			return nil, errors.New(fmt.Sprintf("%v такого ключа не найдено", key))
		}
		return res, nil
	case i.REMOTE:
		res, exists := i.Dirremotebackup[key]
		if !exists {
			return nil, errors.New(fmt.Sprintf("%v такого ключа не найдено", key))
		}
		return res, nil
	}
	return nil, nil
}

//ex : Set(InfoDirectory.REMOTE, keymap, value *FileElement)
func (i *InfoDirectory) Set(typeDir, key string, value *FileElement) error {
	i.Lock()
	defer i.Unlock()
	switch typeDir {
	case i.REMOTE:
		//поиск одинаковых значений
		res, found := i.Dirremotebackup[key]
		if !found {
			//нет такого ключа и слайса соответствено
			i.Dirremotebackup[key] = append(i.Dirremotebackup[key], value)
			return nil
		}
		//ключ найден, поиск по слайсу на вхождение
		for _, x := range res {
			if x.Filename == value.Filename {
				//файл уже есть, скипаю
				return nil
			}
		}
		//нет файла, добавлЯю
		i.Dirremotebackup[key] = append(i.Dirremotebackup[key], value)
	case i.READY:
		//поиск одинаковых значений
		res, found := i.Dirreadybackup[key]
		if !found {
			//нет такого ключа и слайса соответствено
			i.Dirreadybackup[key] = append(i.Dirreadybackup[key], value)
			return nil
		}
		//ключ найден, поиск по слайсу на вхождение
		for _, x := range res {
			if x.Filename == value.Filename {
				//файл уже есть, скипаю
				return nil
			}
		}
		//нет файла, добавлЯю
		i.Dirreadybackup[key] = append(i.Dirreadybackup[key], value)
	}
	return nil
}
func (i *InfoDirectory) ReParseDirectory() error {
	return i.ParseDirectory(i.lastremotebackup, i.lastreadybackup)
}
func (i *InfoDirectory) ParseDirectory(remotebackupdir, readybackupdir string) error {

	//сохраняю
	i.lastremotebackup = remotebackupdir
	i.lastreadybackup = readybackupdir

	//обнуляю мапы
	i.Dirreadybackup = make(map[string][]*FileElement)
	i.Dirremotebackup = make(map[string][]*FileElement)

	//remotebackupdir
	err := filepath.WalkDir(remotebackupdir, func(path string, d fs.DirEntry, err error) error {
		if d != nil {
			if !d.IsDir() {
				abs, _ := filepath.Abs(path)
				dir, filename := filepath.Split(path)
				filenameonly := strings.Split(filename, filepath.Ext(filename))[0]
				aliasdbstock := strings.Split(filepath.Clean(dir), "/")
				ff := aliasdbstock[len(aliasdbstock)-1:][0]
				fi, errfinfo := d.Info()
				if errfinfo != nil {
					i.log.Printf("-- error get fileinfo for %v %v\n", d.Name(), err)
				}
				err := i.Set(i.REMOTE, ff, &FileElement{
					AliasDB:      ff,
					Fullpath:     abs,
					Filename:     filename,
					FilanameOnly: filenameonly,
					item:         fi,
					Pathonly:     filepath.Clean(dir),
					Extension:    filepath.Ext(filename),
				})
				if err != nil {
					return err
				}
			}
		} else {
			i.log.Printf("--WARNING: %v is nil\n", path)
			return errors.New(fmt.Sprintf("--WARNING: %v is nil", path))
		}
		return nil
	})
	if err != nil {
		i.log.Printf("-- [%v] сбор данных по директории ошибка %v\n", remotebackupdir, err)
		return err
	}

	//сохраняющая  => core.cfg.Dirreadybackup
	err = filepath.WalkDir(readybackupdir, func(path string, d fs.DirEntry, err error) error {
		if d != nil {
			if !d.IsDir() {
				abs, _ := filepath.Abs(path)
				dir, filename := filepath.Split(path)
				filenameonly := strings.Split(filename, filepath.Ext(filename))[0]
				aliasdbstock := strings.Split(filepath.Clean(dir), "/")
				ff := aliasdbstock[len(aliasdbstock)-1:][0]
				fi, errfinfo := d.Info()
				if errfinfo != nil {
					i.log.Printf("-- error get fileinfo for %v %v\n", d.Name(), err)
				}
				err := i.Set(i.READY, ff, &FileElement{
					AliasDB:      ff,
					Fullpath:     abs,
					Filename:     filename,
					FilanameOnly: filenameonly,
					item:         fi,
					Pathonly:     filepath.Clean(dir),
					Extension:    filepath.Ext(filename),
				})
				if err != nil {
					return err
				}
			}
		} else {
			i.log.Printf("--WARNING: %v is nil\n", path)
			return errors.New(fmt.Sprintf("--WARNING: %v is nil", path))
		}
		return nil
	})
	if err != nil {
		i.log.Fatalf("-- [%v] сбор данных по директории ошибка %v\n", readybackupdir, err)
		return err
	}

	//проверка на соответствие директорий в in, out;; если нет то создается
	for kin, _ := range i.Dirremotebackup {
		checkdir := filepath.Join(readybackupdir, kin)
		i.log.Printf("----> checkdir: %v : %v\n", checkdir, kin)
		_, found := i.Get(i.READY, kin)
		if found != nil {
			//директории нет в out (ready);; создаю
			err := os.Mkdir(filepath.Join(readybackupdir), os.ModeDir|os.ModePerm) //path to ready dir + aliasdb
			if err != nil {
				i.log.Printf("-- error make directory %v : %v  :%v\n", kin, readybackupdir+"/"+kin, err)
			}
		}
	}

	return nil
}

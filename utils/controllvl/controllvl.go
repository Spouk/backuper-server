package main

import (
	"io/fs"
	"log"
	"os"
	"path/filepath"
	"sort"
	"strings"
)

type node struct {
	Aliasdb  string `json:"aliasdb"`
	Comment  string `json:"comment"`
	Fullpath string `json:"fullpath"`
}
type infoNode struct {
	Jsonfilename string `json:"jsonfilename"`
	ServerBackup string `json:"server_backup"` //server where making backup and run backuper
	ServerDB     string `json:"server_db"`     //ip server dbs
	Aliasdb      string `json:"aliasdb"`
	Counts       int    `json:"counts"` // count archive backups save
	Mode         string `json:"mode"`
	Typeos       string `json:"typeos"`
	InfoFbk      struct {
		Name      string `json:"name"` //namecreate
		Size      int64  `json:"size"`
		Hash      string `json:"hash"`
		StartTime string `json:"start_time"`
		EndTime   string `json:"end_time"`
		LogFile   string `json:"log_file"`
	} `json:"info_fbk"`
	InfoRestore struct {
		Name      string `json:"name"` //namecreate
		Size      int64  `json:"size"`
		Hash      string `json:"hash"`
		StartTime string `json:"start_time"`
		EndTime   string `json:"end_time"`
		LogFile   string `json:"log_file"`
	} `json:"info_restore"`
	Info7z struct {
		Name      string `json:"name"` //namearc
		Size      int64  `json:"size"`
		Hash      string `json:"hash"` //namehash
		StartTime string `json:"start_time"`
		EndTime   string `json:"end_time"`
	} `json:"info_7_z"`
}

type StockBackupFiles struct {
	file7z   string
	filejson string
	info     fs.FileInfo
	aliasdb  string
	pathsrc  string
	pathdest string
	infoNode infoNode
}

func ControlLevelBackup(fullpath string, count int) error {
	var (
		stockfiles = make(map[string][]StockBackupFiles)
		alias      = ""
	)
	err := filepath.Walk(fullpath, func(path string, info fs.FileInfo, err error) error {

		//found dir
		if info.IsDir() {
			alias = filepath.Base(path)
			log.Printf("dir---%v\n", alias)
		}
		// найден файл
		if info.IsDir() == false {
			log.Printf("[%v][ControlLevelBackups] found file in root directory :  %v %v  ", info.Name(), info.ModTime().String(), info.ModTime().UnixNano())
			d, f := filepath.Split(info.Name())
			namefileonly := strings.TrimSuffix(filepath.Base(info.Name()), filepath.Ext(info.Name())) //name file
			fext := filepath.Ext(f)
			//if fext == core.cfg.Extbackup {
			if alias != "" {
				if fext == ".7z" {
					stockfiles[alias] = append(stockfiles[alias], StockBackupFiles{
						file7z:   info.Name(),
						filejson: filepath.Join(d, namefileonly+".json"),
						info:     info})
				}
			} else {
				log.Printf("alias is empty, can adding found files")
			}
		}
		return nil
	})
	if err != nil {
		return err
	}

	for k := range stockfiles {
		log.Printf("alisdb %v\n", alias)
		for _, f := range stockfiles[k] {
			log.Printf("file:  %v\n", f)
		}
	}
	//for k := range stockfiles {
	//		core.log.Printf("[%v][ControlLevelBackups] ----- prefix:  %v ", aliasdb, k)
	//	//sort found files
	//	sort.Slice(stockfiles, func(i, j int) bool {
	//		return stockfiles[i].info.ModTime().UnixNano() > stockfiles[j].info.ModTime().UnixNano()
	//	})
	//
	//	//remove older files
	//	if len(stockfiles) > count {
	//		for _, x := range stockfiles[count:] {
	//			//remove 7z
	//			core.log.Printf("[%v][ControlLevelBackups] removing file -->   %v ", aliasdb, x.file7z)
	//			err := os.Remove(x.file7z)
	//			if err != nil {
	//				core.log.Printf("[%v][ControlLevelBackups] error remove file -->  %v %v ", aliasdb, x.file7z, err)
	//			}
	//			core.log.Printf("[%v][ControlLevelBackups] removing file -->  %v  ", aliasdb, x.filejson)
	//			//remove json
	//			err = os.Remove(x.filejson)
	//			if err != nil {
	//				core.log.Printf("[%v][ControlLevelBackups] error remove file  json -->  %v %v ", aliasdb, x.filejson, err.Error())
	//			}
	//		}
	//	} else {
	//		core.log.Printf("[%v][ControlLevelBackups] count files <  %v no need remove others ", aliasdb, count)
	//	}
	//}
	return nil
}
func HmmDir(basepath string, count int) error {
	var (
		stock         = make(map[string][]string)
		already       = false
		notcorrectdir = []string{}
		//currentNode string
	)
	base := filepath.Base(basepath)
	dirbase := filepath.Dir(basepath)

	log.Printf("base :%v : %v : %v\n", base, dirbase, strings.Split(basepath, "/"))

	err := filepath.WalkDir(basepath, func(path string, d fs.DirEntry, err error) error {
		if err != nil {
			log.Printf("--error inf walkdir function, exit")
			return err
		}
		if d.IsDir() {
			if filepath.Dir(path) == filepath.Dir(basepath) {
				log.Printf("found root directory, skipping  %v :  %v\n", filepath.Dir(path), filepath.Dir(basepath))
			} else {
				//find already in path keys
				for _, x := range strings.Split(path, "/") {
					for k, _ := range stock {
						if x == k {
							log.Printf("---found include directory, skipping...%v : %v\n", path, d.Name())
							already = true
							notcorrectdir = append(notcorrectdir, path)
						}
					}
				}
				if already == false {
					log.Printf("-found dir: [%v] %v : %v  : %v\n", path, d.Name())
					stock[d.Name()] = []string{}
				} else {
					already = false
				}
			}
		} else {
			fdir, ffile := filepath.Split(path)
			found := false
			for _, x := range notcorrectdir {
				if filepath.Dir(fdir) == x {
					found = true
					//log.Printf("-found NOT CORRECT FILE file:%v  : %v   %v  %v \n", d.Name(), path, fdir, ffile)
				}
			}
			if found {
				log.Printf("-found file:%v  : %v   %v  %v \n", d.Name(), path, fdir, ffile)
			}
		}
		return nil
	})

	//show stock found dir in deep 1 lvl
	for k, v := range stock {
		log.Printf("%v : %v \n", k, v)
	}

	for _, v := range notcorrectdir {
		log.Printf("not correct dir: %v\n", v)
	}

	return err
}
func HmmDir2(basepath string, count int) error {
	var (
		stock       = make(map[string][]string)
		countBase   = 0
		correctdirs = []string{}
	)
	base := filepath.Base(basepath)
	dirbase := filepath.Dir(basepath)
	countBase = len(strings.Split(filepath.Dir(basepath), "/"))
	log.Printf("countbase: %v  %v\n", strings.Split(filepath.Dir(basepath), "/"), countBase)
	log.Printf("base :%v : %v : %v\n", base, dirbase, strings.Split(basepath, "/"))

	err := filepath.WalkDir(basepath, func(path string, d fs.DirEntry, err error) error {
		if err != nil {
			log.Printf("--error inf walkdir function, exit")
			return err
		}
		if d.IsDir() {
			if filepath.Dir(path) == filepath.Dir(basepath) {
				//log.Printf("found root directory, skipping  %v :  %v\n", filepath.Dir(path), filepath.Dir(basepath))
			} else {
				//split path for
				//curcount := len(strings.Split(filepath.Dir(path), "/"))
				//log.Printf("%v %v \n", curcount, countBase+count)
				if len(strings.Split(filepath.Dir(path), "/")) <= countBase+count {
					//log.Printf("-found dir: [%v] %v : %v  : %v\n", path, d.Name())
					stock[filepath.Dir(filepath.Join(path, d.Name()))] = []string{}
					correctdirs = append(correctdirs, filepath.Dir(filepath.Join(path, d.Name())))
				}
			}
		} else {
			fdir, ffile := filepath.Split(path)
			for _, x := range correctdirs {
				if x == filepath.Dir(fdir) {
					log.Printf("-found file:%v  : %v   %v  %v \n", d.Name(), path, fdir, ffile)
					stock[x] = append(stock[x], d.Name())
				}
			}
		}

		return nil
	})

	//for _, x := range correctdirs {
	//	log.Printf("cdir: %v\n", x)
	//}

	//show stock found dir in deep 1 lvl
	log.Printf("-----------\n")
	for k, v := range stock {
		log.Printf("%v : %v \n", k, v)
	}

	log.Printf("%v\n", stock)
	return err
}

func main() {
	p := "/backup/backuper/ready"
	count := 1
	countfiles := 2
	extbackup := ".7z"
	_ = ControlDeepWalk2(p, count, countfiles, true, true, extbackup)
}

func ControlDeepWalk2(basepath string, count, countfiles int, hard, debug bool, extbackup string) error {
	var (
		stock       = make(map[string][]fs.DirEntry)
		countBase   = 0
		correctdirs []string
	)
	base := filepath.Base(basepath)
	dirbase := filepath.Dir(basepath)
	countBase = len(strings.Split(filepath.Dir(basepath), "/"))
	if debug {
		log.Printf("countbase: %v  %v\n", strings.Split(filepath.Dir(basepath), "/"), countBase)
		log.Printf("base :%v : %v : %v\n", base, dirbase, strings.Split(basepath, "/"))
	}

	err := filepath.WalkDir(basepath, func(path string, d fs.DirEntry, err error) error {
		if err != nil {
			if debug {
				log.Printf("--error inf walkdir function, exit")
			}
			return err
		}
		if d.IsDir() {
			if filepath.Dir(path) == filepath.Dir(basepath) {
				if debug {
					log.Printf("found root directory, skipping  %v :  %v\n", filepath.Dir(path), filepath.Dir(basepath))
				}
			} else {
				compare := false
				if hard {
					compare = len(strings.Split(filepath.Dir(path), "/")) == countBase+count
				} else {
					compare = len(strings.Split(filepath.Dir(path), "/")) <= countBase+count
				}
				if compare {
					if debug {
						log.Printf("-found dir: [%v] %v\n", path, d.Name())
					}
					stock[filepath.Dir(filepath.Join(path, d.Name()))] = []fs.DirEntry{}
					correctdirs = append(correctdirs, filepath.Dir(filepath.Join(path, d.Name())))
				}
			}
		} else {
			fdir, ffile := filepath.Split(path)
			for _, x := range correctdirs {
				if x == filepath.Dir(fdir) {
					if debug {
						log.Printf("-found file:%v  : %v   %v  %v \n", d.Name(), path, fdir, ffile)
					}
					stock[x] = append(stock[x], d)
				}
			}
		}
		return nil
	})
	if debug {
		for k, v := range stock {
			log.Printf("%v : %v \n", k, v)

		}
		log.Printf("%v\n", stock)
	}
	for k, v := range stock {
		var (
			stockfiles []StockBackupFiles
		)
		log.Printf("%v : %v \n", k, v)
		for _, f := range v {
			//fpath := filepath.Join(k,f.Name()) //fname
			namefileonly := strings.TrimSuffix(filepath.Base(f.Name()), filepath.Ext(f.Name())) //name file only without ext
			fext := filepath.Ext(f.Name())                                                      //fext
			finfo, _ := f.Info()
			if fext == extbackup {
				stockfiles = append(stockfiles, StockBackupFiles{
					file7z:   filepath.Join(k, f.Name()),
					filejson: filepath.Join(k, namefileonly+".json"),
					info:     finfo})
			}
		}

		//sort found files
		sort.Slice(stockfiles, func(i, j int) bool {
			return stockfiles[i].info.ModTime().UnixNano() > stockfiles[j].info.ModTime().UnixNano()
		})

		//remove older files
		if len(stockfiles) > countfiles {
			for _, x := range stockfiles[countfiles:] {
				//remove 7z
				log.Printf("[%v][ControlLevelBackups] removing file -->   %v ", k, x.file7z)
				err := os.Remove(x.file7z)
				if err != nil {
					log.Printf("[%v][ControlLevelBackups] error remove file -->  %v %v ", k, x.file7z, err)
				}
				log.Printf("[%v][ControlLevelBackups] removing file -->  %v  ", k, x.filejson)
				//remove json
				err = os.Remove(x.filejson)
				if err != nil {
					log.Printf("[%v][ControlLevelBackups] error remove file  json -->  %v %v ", k, x.filejson, err.Error())
				}
			}
		} else {
			log.Printf("[%v][ControlLevelBackups] count files <  %v no need remove others ", k, count)
		}
	}

	return err
}

func ControlDeepWalk(basepath string, count int, hard, debug bool) error {
	var (
		stock       = make(map[string][]string)
		countBase   = 0
		correctdirs []string
	)
	base := filepath.Base(basepath)
	dirbase := filepath.Dir(basepath)
	countBase = len(strings.Split(filepath.Dir(basepath), "/"))
	if debug {
		log.Printf("countbase: %v  %v\n", strings.Split(filepath.Dir(basepath), "/"), countBase)
		log.Printf("base :%v : %v : %v\n", base, dirbase, strings.Split(basepath, "/"))
	}

	err := filepath.WalkDir(basepath, func(path string, d fs.DirEntry, err error) error {
		if err != nil {
			if debug {
				log.Printf("--error inf walkdir function, exit")
			}
			return err
		}
		if d.IsDir() {
			if filepath.Dir(path) == filepath.Dir(basepath) {
				if debug {
					log.Printf("found root directory, skipping  %v :  %v\n", filepath.Dir(path), filepath.Dir(basepath))
				}
			} else {
				compare := false
				if hard {
					compare = len(strings.Split(filepath.Dir(path), "/")) == countBase+count
				} else {
					compare = len(strings.Split(filepath.Dir(path), "/")) <= countBase+count
				}
				if compare {
					if debug {
						log.Printf("-found dir: [%v] %v\n", path, d.Name())
					}
					stock[filepath.Dir(filepath.Join(path, d.Name()))] = []string{}
					correctdirs = append(correctdirs, filepath.Dir(filepath.Join(path, d.Name())))
				}
			}
		} else {
			fdir, ffile := filepath.Split(path)
			for _, x := range correctdirs {
				if x == filepath.Dir(fdir) {
					if debug {
						log.Printf("-found file:%v  : %v   %v  %v \n", d.Name(), path, fdir, ffile)
					}
					stock[x] = append(stock[x], d.Name())
				}
			}
		}
		return nil
	})
	if debug {
		for k, v := range stock {
			log.Printf("%v : %v \n", k, v)
		}
		log.Printf("%v\n", stock)
	}
	return err
}

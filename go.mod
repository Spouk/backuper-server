module backuper-server

go 1.17

require (
	github.com/fsnotify/fsnotify v1.6.0
	github.com/go-yaml/yaml v2.1.0+incompatible
	golang.org/x/sys v0.2.0
)

require (
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)

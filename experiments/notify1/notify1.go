package main

import (
	"github.com/fsnotify/fsnotify"
	"log"
	"os"
	"path/filepath"
	"strings"
	"time"
)

func main() {
	//stroka := ".test.json.cfFAdQ"
	//res := strings.Split(stroka, ".")
	//fmt.Println(res, len(res))
	//for _, x := range res {
	//	log.Printf("'%v'\n", x)
	//}
	//os.Exit(1)

	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		log.Fatal(err)
	}
	defer watcher.Close()

	rsync := false

	//timer := time.Second * 2
	done := make(chan bool)
	go func() {
		currentfilename := ""
		for {
			starttimer := time.Now()
			//log.Printf("time now; %v\n", starttimer.String())
			select {
			case <-time.After(3 * time.Second):
				if len(currentfilename) > 0 {
					d, f := filepath.Split(currentfilename)
					ext := filepath.Ext(f)
					log.Printf("%v %v %v %v", currentfilename, d, f, ext)
					currentfilename = ""
				}

			case event, ok := <-watcher.Events:
				starttimer = time.Now()
				currentfilename = event.Name

				if !ok {
					return
				}
				log.Println("event:", event)
				if event.Op&fsnotify.Write == fsnotify.Write {
					log.Println("modified file:", event.Name)
					statwrite, err := os.Stat(event.Name)
					if err != nil {
						log.Println("ERROR GET STAT (rsync = true): ", err)
						rsync = false
					} else {
						log.Printf("stat file: %v    size: %v \n", statwrite.Name(), statwrite.Size())
					}
				}

				if event.Op&fsnotify.Create == fsnotify.Create {
					log.Println("create file:", event.Name)
					stat, err := os.Stat(event.Name)
					if err != nil {
						log.Println("ERROR GET STAT: ", err)
						rsync = false
					} else {
						if stat.IsDir() {
							log.Println("wrong: its directory, skipping...")
							continue
						}
						if rsync {
							stat, err = os.Stat(event.Name)
							if err != nil {
								log.Println("ERROR GET STAT (rsync = true): ", err)
								rsync = false
							} else {
								log.Println("found end event create rsync operation: ", stat.Name(), stat)
								rsync = false
							}
						} else {
							//check rsync start process upload file
							if len(strings.Split(stat.Name(), ".")) == 4 {
								//rsync start upload file
								rsync = true
							} else {
								log.Println("wrong count start rsync process:", strings.Split(stat.Name(), "."))
								rsync = false
							}
						}

						//show summary info stat
						log.Println("stat file:", stat.IsDir(), stat)
						log.Printf("size file : %v\nmodtime: %v\n", stat.Size()/1024/1024, stat.ModTime().String())
					}
				}
			case err, ok := <-watcher.Errors:
				if !ok {
					return
				}
				log.Println("error:", err)
			}
			endtimer := time.Since(starttimer)
			log.Printf("timer since: %v\n", endtimer.String())

		}
	}()

	err = watcher.Add("/stock/experiments")
	if err != nil {
		log.Fatal(err)
	}
	<-done
}

//порядок передачи файлов со стороны клиента
//- file.json = вся информация о архиве
//- file.7z   = сам архив

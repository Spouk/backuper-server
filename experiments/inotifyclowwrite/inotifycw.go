package main

import (
	"bytes"
	"io/fs"
	"log"
	"os"
	"path/filepath"
	"strings"
	"sync"
	"time"
	"unsafe"

	"golang.org/x/sys/unix"
)

type report struct {
	event    string
	filename string
	err      error
}

func monitoring(p string, result chan report) {
	fd, err := unix.InotifyInit1(0)
	if err != nil {
		log.Fatalf("err: %v\n", err)
	}
	defer unix.Close(fd)

	//добавляю события которые хочу отслеживать
	_, err = unix.InotifyAddWatch(
		fd,
		p,
		unix.IN_CREATE|
			unix.IN_DELETE|
			unix.IN_CLOSE_WRITE|
			unix.IN_MOVED_TO|
			unix.IN_MOVED_FROM|
			unix.IN_MOVE_SELF,
	)
	if err != nil {
		//log.Fatalf("err: %v\n", err)
		result <- report{
			event:    "",
			filename: "",
			err:      err,
		}
	}

	var buff [(unix.SizeofInotifyEvent + unix.NAME_MAX + 1) * 20]byte

	for {
		//чтение структуры
		offset := 0
		n, err := unix.Read(fd, buff[:])
		if err != nil {
			//log.Fatalf("err: %v\n", err)
			result <- report{
				event:    "",
				filename: "",
				err:      err,
			}
		}

		for offset < n {
			e := (*unix.InotifyEvent)(unsafe.Pointer(&buff[offset]))

			nameBs := buff[offset+unix.SizeofInotifyEvent : offset+unix.SizeofInotifyEvent+int(e.Len)]
			name := string(bytes.TrimRight(nameBs, "\x00"))
			if len(name) > 0 && e.Mask&unix.IN_ISDIR == unix.IN_ISDIR {
				name += " (dir)"
			}

			switch {
			case e.Mask&unix.IN_CREATE == unix.IN_CREATE:
				result <- report{
					event:    "CREATE",
					filename: filepath.Join(p, name),
					err:      nil,
				}
			case e.Mask&unix.IN_DELETE == unix.IN_DELETE:
				result <- report{
					event:    "DELETE",
					filename: filepath.Join(p, name),
					err:      nil,
				}
			case e.Mask&unix.IN_CLOSE_WRITE == unix.IN_CLOSE_WRITE:
				result <- report{
					event:    "CLOSE_WRITE",
					filename: filepath.Join(p, name),
					err:      nil,
				}
			case e.Mask&unix.IN_MOVED_TO == unix.IN_MOVED_TO:
				result <- report{
					event:    "IN_MOVED_TO",
					filename: filepath.Join(p, name),
					err:      nil,
				}
			case e.Mask&unix.IN_MOVED_FROM == unix.IN_MOVED_FROM:
				result <- report{
					event:    "IN_MOVED_FROM",
					filename: filepath.Join(p, name),
					err:      nil,
				}
			case e.Mask&unix.IN_MOVE_SELF == unix.IN_MOVE_SELF:
				result <- report{
					event:    "IN_MOVE_SELF",
					filename: filepath.Join(p, name),
					err:      nil,
				}
			}
			offset += int(unix.SizeofInotifyEvent + e.Len)
		}
	}
}
func workerManager(p string, sw *sync.WaitGroup, timeWork time.Duration, timebreak time.Duration) {
	log.Printf("[workerManager][%v] start working...", p)
	defer func() {
		log.Printf("[workerManager][%v] end working...", p)
		sw.Done()
	}()
	var (
		//localsw = sync.WaitGroup{}
		result = make(chan report)
	)
	//localsw.Add(1)
	go monitoring(p, result)
	for {
		select {
		case res := <-result:
			if res.err != nil {
				log.Printf("[workerManager][%v] get fatal error from monitoring  exit : %#v\n", p, res)
				return
			}
			log.Printf("[workerManager][%v] get result from monitoring: %#v\n", p, res)
		case <-time.After(timeWork):
			log.Printf("[workerManager][%v] timeout work manager monitoring, the end\n", p)
			return
		}
	}
}
func workerDirListener(d string) {
	var (
		stock []string
	)
	err := filepath.Walk(d, func(path string, info fs.FileInfo, err error) error {
		log.Printf("walkfunc: %v : %v\n", path, info.Name())
		if info.IsDir() {
			if path != d {
				stock = append(stock, path)
			}
		}
		return nil
	})
	if err != nil {
		log.Fatalf("[workerDirListener] fatal error : %v", err)
		return
	}
	print(stock)
	log.Printf("stock:%v\n", stock)
}
func listdir(l []string) map[string][]os.FileInfo {
	result := make(map[string][]os.FileInfo)
	for _, x := range l {
		fd, err := os.Open(x)
		if err != nil {
			log.Printf("error %v %v ", x, err)
			continue
		}
		stockfiles, err := fd.Readdir(-1)
		if err != nil {
			log.Printf("error readdir %v %v", x, err)
		} else {
			var tmpstock []os.FileInfo
			for _, f := range stockfiles {
				if filepath.Ext(f.Name()) == ".7z" {
					tmpstock = append(tmpstock, f)
				}
			}
			result[x] = tmpstock
		}
	}
	return result
}

func main() {
	for k, v := range listdir([]string{"/stock/backup", "/stock/backup/fu", "/stock/backup/tester", "/stock/backup/world"}[1:]) {
		log.Printf("%v  :  %v ", k, v)
	}
	fi := "world-20220327.7z "
	//fname := filepath.Ext(fi)
	log.Println(strings.TrimRight(fi, filepath.Ext(fi)))
	name := strings.Split(fi, filepath.Ext(fi))[0]
	log.Println(name)
	os.Exit(1)

	for {
		select {
		case t := <-time.NewTicker(time.Second * 2).C:
			log.Printf("%v", t.String())
		}
	}

	//alias := strings.Split("/stock/abs/t1", "/stock/abs")
	worker := strings.Join(strings.Split("/stock/abs/t1/ss", "/stock/abs"), "")
	log.Println(worker, len(strings.Split(worker, "/")))

	//workerDirListener("/stock/")
	os.Exit(1)

	var (
		stock []string
	)
	filepath.Walk("/stock/", func(path string, info fs.FileInfo, err error) error {
		log.Printf("walkfunc: %v : %v\n", path, info.Name())
		if info.IsDir() {
			if path != "/stock/" {
				stock = append(stock, path)
			}
		}
		return nil
	})

	var sw = sync.WaitGroup{}
	for _, v := range stock {
		log.Printf("%v", v)
		sw.Add(1)
		go workerManager(v, &sw, time.Second*300, time.Second*3)
	}
	//sw.Add(2)
	//go workerManager("/stock/experiments", &sw, time.Second*300, time.Second*3)
	//go workerManager("/stock/experiments2", &sw, time.Second*300, time.Second*3)
	log.Printf("main awaiting end  work manager monitoring...")
	sw.Wait()
	log.Printf("the end, enjoy dude...")

	////создается хэндлер для прослушивания ядерных уведомление от системного inotify
	//fd, err := unix.InotifyInit1(0)
	//if err != nil {
	//	log.Fatalf("err: %v\n", err)
	//}
	//defer unix.Close(fd)
	//
	////добавляю события которые хочу отслеживать
	//_, err = unix.InotifyAddWatch(
	//	fd,
	//	"/stock/experiments",
	//	unix.IN_CREATE|
	//		unix.IN_DELETE|
	//		unix.IN_CLOSE_WRITE|
	//		unix.IN_MOVED_TO|
	//		unix.IN_MOVED_FROM|
	//		unix.IN_MOVE_SELF|
	//		unix.IN_ALL_EVENTS,
	//)
	//if err != nil {
	//	log.Fatalf("err: %v\n", err)
	//}
	//
	//var buff [(unix.SizeofInotifyEvent + unix.NAME_MAX + 1) * 20]byte
	//
	////https://linuxhint.com/inotify_api_c_language/
	////struct inotify_event {
	////	int32t   wd;
	////	uint32_t  mask;
	////	uint32_t  cookie;
	////	uint32_t  len;
	////	char name[];
	////}
	//for {
	//	//чтение структуры
	//	offset := 0
	//	n, err := unix.Read(fd, buff[:])
	//	if err != nil {
	//		log.Fatalf("err: %v\n", err)
	//	}
	//	log.Printf("event struct read: %v\n", n)
	//
	//	log.Printf("offset before: %v\n", offset)
	//
	//	for offset < n {
	//		e := (*unix.InotifyEvent)(unsafe.Pointer(&buff[offset]))
	//
	//		nameBs := buff[offset+unix.SizeofInotifyEvent : offset+unix.SizeofInotifyEvent+int(e.Len)]
	//		name := string(bytes.TrimRight(nameBs, "\x00"))
	//		if len(name) > 0 && e.Mask&unix.IN_ISDIR == unix.IN_ISDIR {
	//			name += " (dir)"
	//		}
	//
	//		switch {
	//		case e.Mask&unix.IN_CREATE == unix.IN_CREATE:
	//			fmt.Printf("CREATE %v\n", name)
	//		case e.Mask&unix.IN_DELETE == unix.IN_DELETE:
	//			fmt.Printf("DELETE %v\n", name)
	//		case e.Mask&unix.IN_CLOSE_WRITE == unix.IN_CLOSE_WRITE:
	//			fmt.Printf("CLOSE_WRITE %v\n", name)
	//		case e.Mask&unix.IN_MOVED_TO == unix.IN_MOVED_TO:
	//			fmt.Printf("IN_MOVED_TO [%v] %v\n", e.Cookie, name)
	//		case e.Mask&unix.IN_MOVED_FROM == unix.IN_MOVED_FROM:
	//			fmt.Printf("IN_MOVED_FROM [%v] %v\n", e.Cookie, name)
	//		case e.Mask&unix.IN_MOVE_SELF == unix.IN_MOVE_SELF:
	//			fmt.Printf("IN_MOVE_SELF %v\n", name)
	//		}
	//
	//		offset += int(unix.SizeofInotifyEvent + e.Len)
	//		log.Printf("offset after: %v\n", offset)
	//	}
	//}
}

package main

import (
	"github.com/fsnotify/fsnotify"
	"log"
	"os"
	"time"
)

func main() {
	com := make(chan string)
	counter := 10
	go notifyListenerDir(com, "/tmp")

	for counter > 0 {
		time.Sleep(time.Second * 4)
		counter--
	}
	com <- "exit"
	time.Sleep(1)
	log.Println("--the end example --")
	os.Exit(1)

	// Create new watcher.
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		log.Fatal(err)
	}
	defer func(watcher *fsnotify.Watcher) {
		err := watcher.Close()
		if err != nil {
			log.Println(err)
		}
	}(watcher)

	// Start listening for events.
	go func() {
		for {
			select {
			case event, ok := <-watcher.Events:
				if !ok {
					return
				}
				log.Println("event:", event)
				if event.Has(fsnotify.Write) {
					log.Println("modified file:", event.Name)
				}
				if event.Has(fsnotify.Create) {
					log.Println("create file:", event.Name)
					stat, err := os.Stat(event.Name)
					if err != nil {
						log.Println("error get stat ", event.Name, err)
						continue
					}
					if stat.IsDir() {
						log.Println("bingo, this directory: ", event.Name)
					}

				}
			case err, ok := <-watcher.Errors:
				if !ok {
					return
				}
				log.Println("error:", err)
			}
		}
	}()

	// Add a path
	err = watcher.Add("/tmp")
	if err != nil {
		log.Fatal(err)
	}

	// Block main goroutine forever.
	<-make(chan struct{})
}

func notifyListenerDir(com chan string, listenDir string) {
	defer func() {
		log.Println("--exiting notifyListenerDir")
	}()
	log.Println("--starting notifyListenerDir")

	// Create new watcher.
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		log.Fatal(err)
	}
	defer func(watcher *fsnotify.Watcher) {
		err := watcher.Close()
		if err != nil {
			log.Println(err)
		}
	}(watcher)

	err = watcher.Add(listenDir)
	if err != nil {
		log.Println("error set directory for watching!!!!!, exit listener....")
		return
	}

	for {
		select {
		case command := <-com:
			switch command {
			case "exit":
				log.Printf("--getting command to exit, cya dude ;)")
				return
			}
		case event, ok := <-watcher.Events:
			if !ok {
				log.Println("-- error get events from watcher...")
				continue
			}
			log.Println("event:", event)
			if event.Has(fsnotify.Write) {
				log.Println("modified file:", event.Name)
			}
			if event.Has(fsnotify.Create) {
				log.Println("create file:", event.Name)
				stat, err := os.Stat(event.Name)
				if err != nil {
					log.Println("error get stat ", event.Name, err)
					continue
				}
				if stat.IsDir() {
					log.Println("bingo, this directory: ", event.Name)
				}

			}
		case err, ok := <-watcher.Errors:
			if !ok {
				log.Println("-- error get events from watcher...", err)
				continue
			}
		}
	}

}

package main

import (
	"fmt"
	"log"
	"os"
	"sync"
	"time"
)

type stocker struct {
	listids [3]int
}

func main() {
	sw := new(sync.WaitGroup)
	sw.Add(2)
	go worker1(sw, 1, time.Duration(2))
	go worker1(sw, 2, time.Duration(4))
	sw.Wait()
	log.Printf("--end work main....\n")
	os.Exit(1)
	//n, err := time.Parse("3:04PM", time.Now().Format(time.Kitchen))
	//if err != nil {
	//	log.Println("error ", err)
	//} else {
	//	log.Println(n.Format(time.Kitchen), err)
	//}
	nn, err := time.Parse("15:04:05", "23:01:00")
	fmt.Printf("result : %v %v", nn.Format("15:04:05"), err)
	fmt.Println()

	var (
		layer = "15:04:05"
	)
	startime, err := time.Parse(layer, "20:00:00")
	if err != nil {
		panic(err)
	}
	ct := time.Now()
	cort := time.Date(ct.Year(), ct.Month(), ct.Day(), startime.Hour(), startime.Minute(), startime.Second(), startime.Nanosecond(), ct.Location())
	fmt.Printf("%v\n%v\n", ct.String(), cort.String())
	os.Exit(1)

	//current time in format hour:minute:second
	curtime, _ := time.Parse(layer, time.Now().Format(layer))
	fmt.Printf("starttime: %v\ncurtime: %v\n", startime.String(), curtime.String())
	correctstart := startime.Add(time.Hour + time.Duration(5)).Unix()
	fmt.Printf("correct: %v, %v", correctstart, correctstart > curtime.Unix())
	//looper()
	//runner("20:30:20", time.Hour*3)

}
func worker1(sw *sync.WaitGroup, id int, period time.Duration) {
	defer func() {
		log.Printf("-worker%v end work\n", id)
		sw.Done()
		return
	}()
	ticker := time.NewTicker(period * time.Second)
	for {
		select {
		case _ = <-ticker.C:
			log.Printf("-worker%v start work....\n", id)
			time.Sleep(1)
			log.Printf("-worker%v end  work, going sleep\n", id)
		default:
			continue
		}
	}
}

//"20:00:00"
func convertTime(tstr string) time.Time {
	var (
		layer = "15:04:05"
	)
	startime, err := time.Parse(layer, tstr)
	if err != nil {
		panic(err)
	}
	ct := time.Now()
	cort := time.Date(ct.Year(), ct.Month(), ct.Day(), startime.Hour(), startime.Minute(), startime.Second(), startime.Nanosecond(), ct.Location())
	fmt.Printf("%v\n%v\n", ct.String(), cort.String())
	return cort
}

//start time = string from config file == '20:20:00'
func runner(starttime string, validcorrection time.Duration) {
	//get current time
	var (
		layer = "15:04:05"
	)
	sttime, err := time.Parse(layer, starttime)
	if err != nil {
		panic(err)
	}
	//current time in format hour:minute:second
	curtimer, _ := time.Parse(layer, time.Now().Format(layer))

	sw := sync.WaitGroup{}

	if sttime.Unix() > curtimer.Unix() {
		//sleep programm for await need timer for run looper
		log.Printf("--start sleeping for : %v minutes", sttime.Sub(curtimer).Minutes())
		time.Sleep(time.Duration(sttime.Sub(curtimer).Minutes()))
		log.Printf("--awaiting from  sleeping for...")
		//run singlew worker for now day
		sw.Add(1)
		go fixworker()
		//run main looper
		sw.Add(1)
		go looper()
		log.Printf("--running worker and main looper...")
		sw.Wait()
	} else {
		if curtimer.Sub(sttime).Hours() <= validcorrection.Hours() {
			//valid time period for run single worker
		} else {
			//not valid, only sleep
		}
	}

	fmt.Printf("sttime: %v %v  %v\n", sttime.Unix(), curtimer.Unix()-sttime.Unix(), curtimer.Unix() > sttime.Unix())
	fmt.Printf("correct convert time: %v %v ",
		sttime.Format(layer), sttime.String())

	//calcualate period for start looper

}
func fixworker() {
	log.Println("startinng fixworker")
	time.Sleep(time.Second)
	defer func() {
		log.Println("end work fixworker")
	}()
}

//main function
func looper() {
	log.Println("looper starting...")
	defer func() {
		log.Println("looper end working...")
	}()
	ticker := time.NewTicker(time.Second * 10)
	timer := time.NewTimer(time.Second * 10)
	for {
		select {
		case ti := <-timer.C:
			log.Printf("timer: %v", ti)
		case res := <-ticker.C:
			log.Printf("ticker : %v", res.String())
		default:
			continue
		}
	}
}

//// брать время текущего запуска программы и от него отсчитывать время до начала и до конца
//если стартовое времся тикера = 20 00  то при запуске в 18 00
//алгоритм обработки должен быть следующий
//if currenttime < startticker then
//  starttimer - currenttime = timeforpause
//  time.Sleep(timeforpause)
//  or
//  time.afterfunc(timforpause, funcmainlooper include startticker)
//
//  то есть программа впадает в спячку и активизируется только по наступлении startticker
//
//особенности
// * тело тикера будет запущено первый раз по истечении времени указаного в тикере то есть если запускать его в 2000 0101 то первый раз он
//   запуститьс в 2000 0202 !!!!
//	по это причине этот момент надо перекрывать запуском одиночного таймера
//	в рамках которого запускать функцию по бэкапированию и прочего хозяйства
//	или запускать основной цикл тикера ровно в 20 00 используя time.afterfunc
//    а до этого программу держать в спячке time.sleep до 20
//  после выхода из спячки в 20 00
// запускается отдельная горутина по формированию бэкапа
//и тут же запускается цикл for select с основным тикером 24 часа
//
//
//// 2 периода меньше 20 и больше 20
//если меньше 20 то

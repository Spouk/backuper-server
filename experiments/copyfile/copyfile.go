package main

import (
	"flag"
	"io"
	"log"
	"os"
)

func main() {
	srcfile := flag.String("src", "", "source file with full path")
	dstfile := flag.String("dst", "", "dest file with full path")
	flag.Parse()
	if len(*dstfile) == 0 || len(*srcfile) == 0 {
		flag.PrintDefaults()
		os.Exit(1)
	}
	cr, cw, err := copyfile(*srcfile, *dstfile)
	if err != nil {
		log.Fatal(err)
	} else {
		log.Printf("success copy file, count read: %d count write: %d \n", cr, cw)
	}
}
func copyfile(src, dst string) (int, int, error) {
	buf := make([]byte, 1024)
	countread, countwrite := 0, 0

	fin, err := os.Open(src)
	if err != nil {
		return -1, -1, err
	}
	fout, err := os.Create(dst)
	if err != nil {
		return -1, -1, err
	}

	for {
		n, err := fin.Read(buf)
		countread += n
		if err != nil && err != io.EOF {
			log.Println("error read file ", err)
			return countread, countwrite, err
		}
		if n == 0 {
			break
		}
		nw, err := fout.Write(buf[:n])
		if err != nil {
			log.Println("error write file ", err)
			return countread, countwrite, err
		}
		countwrite += nw
	}
	return countread, countwrite, nil
}
